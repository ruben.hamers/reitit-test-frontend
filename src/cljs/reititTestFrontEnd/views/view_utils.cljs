(ns reititTestFrontEnd.views.view-utils
  (:require
    [re-frame.core :as rf]
    [reagent.core :as r]
    [re-com.core :as rc]
    [breaking-point.core :as bp]
    [reititTestFrontEnd.events.mainEvents :as main-events]
    [reititTestFrontEnd.events.filesEvents :as files-events]
    [reititTestFrontEnd.config :refer [environment]]
    [ajax.core :as ajax]))

(defn prepare-form-data [element-id event]
  (let [el (.getElementById js/document element-id)
        name (.-name el)
        file (aget (.-files el) 0)
        form-data (doto
                    (js/FormData.)
                    (.append name file))]
    (rf/dispatch [event form-data])))

(defn file-upload-form [form-id input-id]
  [:div
   [:form {:id       form-id
           :enc-type "multipart/form-data"
           :method   "POST"}
    [:input {:type "file"
             :name "file"
             :id   input-id}]]])

(defn display-alert-box [event type to-dissoc display-fn]
  (let [success-files-upload (rf/subscribe [event])]
    (when-let [files @success-files-upload]
      (do
        (rf/dispatch [:delete-db-key to-dissoc])
        [rc/alert-box
         :alert-type type
         :body (display-fn files)
         ]
        ))))
(ns reititTestFrontEnd.views.files.files
  (:require
    [re-frame.core :as rf]
    [reagent.core :as r]
    [re-com.core :as rc]
    [breaking-point.core :as bp]
    [reititTestFrontEnd.events.mainEvents :as main-events]
    [reititTestFrontEnd.events.filesEvents :as files-events]
    [reititTestFrontEnd.config :refer [environment]]
    [reititTestFrontEnd.views.view-utils :as utils]
    [ajax.core :as ajax]))

;;main stuff
(defn- files-title []
  [rc/title
   :label "This is the file page. You can test uploading a file or download the example image from the reitit test api"
   :level :level1])

(defn- link-to-home-page []
  [rc/hyperlink-href
   :label "go to Home Page"
   :href "#/"])
;;upload
(defn- files-upload []
    [rc/v-box
     :children [
                [rc/label :label "Upload a file to the backend"]
                (utils/file-upload-form "upload-form" "upload-file")
                [rc/button
                 :label "Upload"
                 :on-click #(utils/prepare-form-data "upload-file" :request-files-upload)
                 :class "btn-enabled"]]])

(defn- files-download []
  [rc/v-box
   :children [
              [rc/label :label "Download the example image from the backend"]
              [rc/button
               :label "Download"
               :on-click #(rf/dispatch [:request-files-download])
               :class "btn-enabled"]]])


(defn files-panel []
  [rc/v-box
   :gap "1em"
   :children [[files-title]
              [files-upload] ;event type to-dissoc display-fn
              (utils/display-alert-box ::files-events/success-files-upload-result :info :failed-files-upload-result (fn [files] (str "Thank you for uploading" files)))
              (utils/display-alert-box ::files-events/failed-files-upload-result :danger :success-files-upload-result (fn [files] files))
              [files-download]
              (utils/display-alert-box ::files-events/success-files-download-result :info :failed-files-download-result (fn [files] files))
              (utils/display-alert-box ::files-events/failed-files-download-result :danger :success-files-download-result (fn [files] files))
              [link-to-home-page]]])
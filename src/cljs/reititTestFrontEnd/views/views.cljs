(ns reititTestFrontEnd.views.views
  (:require
   [re-frame.core :as re-frame]
   [re-com.core :as re-com]
   [breaking-point.core :as bp]
   [reititTestFrontEnd.events.mainEvents :as main-events]
   [reititTestFrontEnd.views.home.home :as home]
   [reititTestFrontEnd.views.about.about :as about]
   [reititTestFrontEnd.views.math.math :as math]
   [reititTestFrontEnd.views.files.files :as files]
   ))

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home/home-panel]
    :about-panel [about/about-panel]
    :math-panel [math/math-panel]
    :files-panel [files/files-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::main-events/active-panel])]
    [re-com/v-box
     :height "100%"
     :children [[panels @active-panel]]]))
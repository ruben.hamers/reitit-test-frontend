(ns reititTestFrontEnd.views.home.home
  (:require
    [re-frame.core :as re-frame]
    [re-com.core :as re-com]
    [breaking-point.core :as bp]
    [reititTestFrontEnd.events.mainEvents :as main-events]
    ))

(defn- display-re-pressed-example []
  (let [re-pressed-example (re-frame/subscribe [::main-events/re-pressed-example])]
    [:div

     [:p
      [:span "Re-pressed is listening for keydown events. A message will be displayed when you type "]
      [:strong [:code "hello"]]
      [:span ". So go ahead, try it out!"]]

     (when-let [rpe @re-pressed-example]
       [re-com/alert-box
        :alert-type :info
        :body rpe])]))

(defn- home-title []
  (let [name (re-frame/subscribe [::main-events/name])]
    [re-com/title
     :label (str "Hello from " @name ". This is the Home Page.")
     :level :level1]))

(defn- link-to-about-page []
  [re-com/hyperlink-href
   :label "go to About Page"
   :href "#/about"])

(defn- link-to-files-page []
  [re-com/hyperlink-href
   :label "go to Files Page"
   :href "#/files"])

(defn- link-to-math-page []
  [re-com/hyperlink-href
   :label "go to Math Page"
   :href "#/math"])

(defn home-panel []
  [re-com/v-box
   :gap "1em"
   :children [[home-title]
              [link-to-about-page]
              [link-to-files-page]
              [link-to-math-page]
              [display-re-pressed-example]
              [:div
               [:h3 (str "screen-width: " @(re-frame/subscribe [::bp/screen-width]))]
               [:h3 (str "screen: " @(re-frame/subscribe [::bp/screen]))]]
              ]])
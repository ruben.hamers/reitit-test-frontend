(ns reititTestFrontEnd.views.math.math
  (:require
    [re-frame.core :as rf]
    [reagent.core :as r]
    [re-com.core :as rc]
    [breaking-point.core :as bp]
    [reititTestFrontEnd.events.mainEvents :as main-events]
    [reititTestFrontEnd.events.mathEvents :as math-events]
    [reititTestFrontEnd.config :refer [environment]]
    ))

(defn- math-title []
  [rc/title
   :label "This is the Math Page. You can test against the reitit-spec-swagger api from here."
   :level :level1])

(defn- math-get []
  (let [x (r/atom nil)
        y (r/atom nil)]
    [rc/v-box
     :children [
                [rc/label :label "Math Plus"]
                [rc/input-text
                 :model x
                 :placeholder "math x"
                 :width "300px"
                 :on-change #(reset! x %)]
                [rc/input-text
                 :model y
                 :placeholder "math y"
                 :width "300px"
                 :on-change #(reset! y %)]
                [rc/h-box
                 :children [
                            [rc/button
                             :label "Get"
                             :on-click #(rf/dispatch [:request-math-get {:x @x :y @y}])
                             :class "btn-enabled"]
                            [rc/button
                             :label "Post"
                             :on-click #(rf/dispatch [:request-math-post {:x (int @x) :y (int @y)}])
                             :class "btn-enabled"]]
                 ]]]))

(defn- display-math-get-success []
  (let [success-get-math-plus (rf/subscribe [::math-events/success-math-get-result])]
    (when-let [number @success-get-math-plus]
      (do
        (rf/dispatch [:delete-db-key :failed-math-get-result])
        [rc/alert-box
         :alert-type :info
         :body (str "total = " (:total number))
         ]
        ))))

(defn- display-math-get-fail []
  (let [failed-get-math-plus (rf/subscribe [::math-events/failed-math-get-result])]
    (when-let [failed-request @failed-get-math-plus]
      (do
        (rf/dispatch [:delete-db-key :success-math-get-result])
        [rc/alert-box
       :alert-type :danger
       :body failed-request]
      ))))

(defn- link-to-home-page []
  [rc/hyperlink-href
   :label "go to Home Page"
   :href "#/"])

(defn math-panel []
  [rc/v-box
   :gap "1em"
   :children [[math-title]
              [math-get]
              [display-math-get-success]
              [display-math-get-fail]
              [link-to-home-page]]])
(ns reititTestFrontEnd.events.filesEvents
  (:require
    [re-frame.core :as rf]
    [reititTestFrontEnd.config :refer [environment]]
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [day8.re-frame.http-fx]
    [ajax.core :as ajax]
    ))

;; subs
;;upload
(rf/reg-sub
  ::success-files-upload-result
  (fn [db]
    (let [data (:success-files-upload-result db)]
      data)))

(rf/reg-sub
  ::failed-files-upload-result
  (fn [db]
    (when-let [data (:failed-files-upload-result db)]
      (do (dissoc db :failed-files-upload-result)
          "Oops, something went wrong!"))))

;;download
(rf/reg-sub
  ::success-files-download-result
  (fn [db]
    (let [data (:success-files-download-result db)]
      data)))

(rf/reg-sub
  ::failed-files-download-result
  (fn [db]
    (when-let [data (:failed-files-download-result db)]
      (do (dissoc db :failed-files-download-result)
          "Oops, something went wrong!"))))

;; Events
;;Upload
(rf/reg-event-db
  ::good-files-upload-result
  (fn [db [_ result]]
    (assoc db :success-files-upload-result result)))

(rf/reg-event-db
  ::bad-files-upload-result
  (fn [db [_ result]]
    (assoc db :failed-files-upload-result result)))

;;Download
(rf/reg-event-db
  ::good-files-download-result
  (fn [db [_ result]]
    (assoc db :success-files-download-result result)))

(rf/reg-event-db
  ::bad-files-download-result
  (fn [db [_ result]]
    (assoc db :failed-files-download-result result)))

;;Requests

(rf/reg-event-fx
  :request-files-upload
  (fn-traced [{:keys [db event]} [_ a]]
             (let [params (second event)
                   url (str (:backend-url environment) "/files/upload")]
               (println params)
               {:http-xhrio {:method           :post
                             :uri              url
                             :timeout          8000
                             :response-format  (ajax/json-response-format {:keywords? true})
                             :body params
                             :on-success       [::good-files-upload-result]
                             :on-failure       [::bad-files-upload-result]}})))
(rf/reg-event-fx
  :request-files-download
  (fn-traced [{:keys [db event]} [_ a]]
             {:http-xhrio {:method          :get
                           :uri             (str (:backend-url environment) "/files/download")
                           :timeout         8000
                           :format          (ajax/json-request-format)
                           :response-format (ajax/raw-response-format)
                           :on-success      [::good-files-download-result]
                           :on-failure      [::bad-files-download-result]}}))
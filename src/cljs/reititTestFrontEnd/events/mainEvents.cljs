(ns reititTestFrontEnd.events.mainEvents
  (:require
   [re-frame.core :as rf]
   [reititTestFrontEnd.db :as db]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [reititTestFrontEnd.events.filesEvents]
   [reititTestFrontEnd.events.mathEvents]
   )
  )

;; subs
(rf/reg-sub
  ::name
  (fn [db]
    (:name db)))

(rf/reg-sub
  ::active-panel
  (fn [db _]
    (:active-panel db)))

(rf/reg-sub
  ::re-pressed-example
  (fn [db _]
    (:re-pressed-example db)))

;; Events

(rf/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(rf/reg-event-db
 ::set-active-panel
 (fn-traced [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(rf/reg-event-db
 ::set-re-pressed-example
 (fn [db [_ value]]
   (assoc db :re-pressed-example value)))

(rf/reg-event-db
  :delete-db-key
  (fn
    [db [_ key-to-delete]]
    (if (contains? db key-to-delete)
      (dissoc db key-to-delete)
      db)))
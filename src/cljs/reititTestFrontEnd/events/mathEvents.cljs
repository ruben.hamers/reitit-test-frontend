(ns reititTestFrontEnd.events.mathEvents
  (:require
    [re-frame.core :as rf]
    [reititTestFrontEnd.config :refer [environment]]
    [day8.re-frame.tracing :refer-macros [fn-traced]]
    [day8.re-frame.http-fx]
    [ajax.core :as ajax]
    ))
;; subs
(rf/reg-sub
  ::success-math-get-result
  (fn [db]
    (let [data (:success-math-get-result db)]
      (dissoc db :success-math-get-result)
      data)))

(rf/reg-sub
  ::failed-math-get-result
  (fn [db]
    (when-let [data (:failed-math-get-result db)]
      (do (dissoc db :failed-math-get-result)
          "Oops, something went wrong!"))))

;;Events
(rf/reg-event-db
  ::good-http-result
  (fn [db [_ result]]
    (assoc db :success-math-get-result result)))

(rf/reg-event-db
  ::bad-http-result
  (fn [db [_ result]]
    (assoc db :failed-math-get-result result)))

(rf/reg-event-fx
  :request-math-get
  (fn-traced [{:keys [db event]} [_ a]]
             (let [params (second event)
                   url (str (:backend-url environment) "/math/plus")]
               {:http-xhrio {:method          :get
                             :uri             url
                             :timeout         8000
                             :format          (ajax/json-request-format)
                             :response-format (ajax/json-response-format {:keywords? true})
                             :params          params
                             :on-success      [::good-http-result]
                             :on-failure      [::bad-http-result]}})))
(rf/reg-event-fx
  :request-math-post
  (fn-traced [{:keys [db event]} [_ a]]
             (let [params (second event)
                   url (str (:backend-url environment) "/math/plus")]
               {:http-xhrio {:method          :post
                             :uri             url
                             :timeout         8000
                             :format          (ajax/json-request-format)
                             :response-format (ajax/json-response-format {:keywords? true})
                             :params          params
                             :on-success      [::good-http-result]
                             :on-failure      [::bad-http-result]}})))
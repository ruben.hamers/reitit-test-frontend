(ns reititTestFrontEnd.routes.files
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
    [re-frame.core :as re-frame]
    [re-pressed.core :as rp]
    [reititTestFrontEnd.events.mainEvents :as events]
    ))

(def routes
  (defroute "/files" []
            (re-frame/dispatch [::events/set-active-panel :files-panel])))

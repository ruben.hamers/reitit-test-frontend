(ns reititTestFrontEnd.routes.about
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
    [re-frame.core :as re-frame]
    [re-pressed.core :as rp]
    [reititTestFrontEnd.events.mainEvents :as events]
    ))

(def routes
  (defroute "/about" []
            (re-frame/dispatch [::events/set-active-panel :about-panel])))

(ns reititTestFrontEnd.routes.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
   [secretary.core :as secretary]
   [goog.events :as gevents]
   [reititTestFrontEnd.routes.home :as home]
   [reititTestFrontEnd.routes.about :as about]
   [reititTestFrontEnd.routes.files :as files]
   [reititTestFrontEnd.routes.math :as math]
   ))

(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))


(defn app-routes []
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  (home/routes)
  (about/routes)
  (files/routes)
  (math/routes)
  ;;----middleware-------
  ;; --------------------
  (hook-browser-navigation!))

(ns reititTestFrontEnd.routes.math
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
    [re-frame.core :as re-frame]
    [re-pressed.core :as rp]
    [reititTestFrontEnd.events.mainEvents :as events]
    ))

(def routes
  (defroute "/math" []
            (re-frame/dispatch [::events/set-active-panel :math-panel])))

(ns reititTestFrontEnd.routes.home
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
    [re-frame.core :as re-frame]
    [re-pressed.core :as rp]
    [reititTestFrontEnd.events.mainEvents :as events]
    ))


(def routes
  (defroute "/" []
            (re-frame/dispatch [::events/set-active-panel :home-panel])
            (re-frame/dispatch [::events/set-re-pressed-example nil])
            (re-frame/dispatch
              [::rp/set-keydown-rules
               {:event-keys [[[::events/set-re-pressed-example "Hello, world!"]
                              [{:keyCode 72}                ;; h
                               {:keyCode 69}                ;; e
                               {:keyCode 76}                ;; l
                               {:keyCode 76}                ;; l
                               {:keyCode 79}                ;; o
                               ]]]

                :clear-keys
                            [[{:keyCode 27}                 ;; escape
                              ]]}])
            ))
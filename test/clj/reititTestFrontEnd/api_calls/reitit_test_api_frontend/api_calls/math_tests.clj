(ns reitit-test-api-frontend.api-calls.math-tests
  (require
    [clojure.test :refer [deftest testing is]]
    [reititTestFrontEnd.api-calls.math :as math]))

(deftest math-get-test
  (testing "test if the predefined api-call works as expected"
    (let [json (math/get-math 1 2)]
      (println json)
      (is (= true true)))))
